﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaxCalculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Click on the parameters to get specific help.");
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            decimal inc;
            decimal tax;
            decimal church;
            Decimal.TryParse(textBox1.Text, out inc);
            Decimal.TryParse(textBox2.Text, out tax);
            Decimal.TryParse(textBox3.Text, out church);
            decimal result = Calculator.CalculateTax(inc, tax, church, true);
            //MessageBox.Show(result.ToString());
            textBox4.Text = result.ToString();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void label2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Here you write your tax rate. E.g.: 0,37.");
        }

        private void label1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Here you write your income. E.g.: 10000.");
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("TaxCalculator v1.0.1 - Created by Nikolaj Z. Lollike, nlol@itu.dk - Copyright 2013.");
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("What you have earned after skattedaddy has taken all yo dollars.");
        }

        private void label3_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show("Here you write your Church Tax. E.g.: 0,008.");
        }
    }
}
